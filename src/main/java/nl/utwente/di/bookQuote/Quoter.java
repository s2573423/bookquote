package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {

	private HashMap<String, Double> bookPrice = new HashMap<String, Double>();

	public double getBookPrice(String parameter) {

		bookPrice.put("1", 10.0);
		bookPrice.put("2", 45.0);
		bookPrice.put("3", 20.0);
		bookPrice.put("4", 35.0);
		bookPrice.put("5", 50.0);
		bookPrice.put("others", 0.0);
		Double price = bookPrice.get(parameter);
		return price;
	}
}
